#!/usr/bin/env python3

import os
import gitlab
import sys
import yaml
from colorama import Fore, Style


def main():
    private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
    if not private_token:
        print(Fore.RED + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!' + Style.RESET_ALL)
        sys.exit(1)
    path = os.getenv('CI_PROJECT_PATH')
    if not path:
        print(Fore.RED + 'ERROR: project path not found CI_PROJECT_PATH!' + Style.RESET_ALL)
        sys.exit(1)
    gl = gitlab.Gitlab('https://gitlab.com', api_version=4,
                       private_token=private_token)
    project = gl.projects.get(path, lazy=True)

    labels = []
    for page in range(12):
        for label in project.labels.list(page=page, per_page=100):
            if not label.is_project_label:
                continue
            output = dict()
            for k, v in label.attributes.items():
                if not v or k in ('is_project_label', 'text_color', 'project_id'):
                    continue
                output[k] = v
            labels.append(output)
    with open(path.replace('/', '-') + '-labels-backup.yaml', 'w') as fp:
        yaml.dump(labels, fp, default_flow_style=False)


if __name__ == "__main__":
    main()
