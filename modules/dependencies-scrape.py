#!/usr/bin/env python3

import inspect
import os
import re
import sys


localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..'))
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule
from fdroidserver import common, metadata, scanner


class DependenciesScrape(IssuebotModule):
    def main(self):
        build = metadata.Build()
        build.gradle = ['([a-z0-9]*)']  # use a pattern to match all flavors
        dependency_regexs = []
        for regex in scanner.get_gradle_compile_commands(build):
            dependency_regexs.append(
                re.compile(r'^' + regex.pattern + r"""\s*\(?['"](\S+)['"]\)?.*""", re.IGNORECASE)
            )

        paths = common.get_all_gradle_and_manifests(self.source_dir)
        output = dict()
        os.chdir(self.source_dir)
        for path in paths:
            if path.endswith('AndroidManifest.xml'):
                continue
            current_file = os.path.relpath(path, self.source_dir)
            if current_file not in output:
                output[current_file] = []
            with open(current_file) as fp:
                linenum = 0
                for line in fp.readlines():
                    linenum += 1
                    for regex in dependency_regexs:
                        for m in regex.finditer(line):
                            if m.lastindex == 1:
                                gradle_line = m.group(1)
                                flavor = ''
                            else:
                                gradle_line = m.group(2)
                                flavor = m.group(1)
                            output[current_file].append((gradle_line, linenum, flavor))

        report = ''
        for f, libraries in sorted(output.items(), key=lambda i: (len(i[0].split('/')), i[0])):
            relpath = os.path.relpath(f, self.source_dir)
            url = self.get_source_url(relpath)
            fileentry = ''
            for lib, linenum, flavor in sorted(libraries):
                fileentry += (
                    '''<tr><td><a href="{url}"><code>{lib}</code></a></td><td><tt>{flavor}</tt></td></tr>\n'''
                    .format(url=url + '#L' + str(linenum), lib=lib, flavor=flavor)
                )
            if fileentry:
                report += ('<details><summary><tt>%s</tt></summary><table>' % relpath)
                report += '<tr><td><u>dependency</u></td><td><u>gradle flavor</u></td></tr>'
                report += fileentry
                report += '</table></details>'
        if report:
            self.reply['report'] = (
                self.get_source_scanning_header('Dependencies scraped from gradle files')
                + report
            )
        self.reply['reportData'] = output
        self.write_json()


if __name__ == "__main__":
    DependenciesScrape().main()
